import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import './router/auth';
import vuetify from './plugins/vuetify';
import i18n from './plugins/i18n';
import VCharts from 'v-charts';
import '../src/styles/index.scss';

Vue.use(VCharts);

Vue.config.productionTip = false;

new Vue({
    router,
    i18n,
    store,
    vuetify,
    render: (h) => h(App)
}).$mount('#app');
