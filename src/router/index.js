import Vue from 'vue';
import Router from 'vue-router';

import layout from '@/layout/layout.vue';
// import treeRoute from '@/views/layout/router.vue';

Vue.use(Router);
export default new Router({
    // mode:'history',
    routes: [
        //首页
        {
            path: '/',
            redirect: '/login',
            name: 'index',
            visible: false,
            component: layout,
            meta: {
                title: 'home',
                keepAlive: false
            }
        },
        // Nest Mining
        {
            path: '/nest',
            visible: true,
            component: layout,
            meta: {
                title: 'NestMining',
                icon: 'mdi-gauge',
                keepAlive: false
            },
            children: [
                {
                    path: '',
                    name: 'nestmining',
                    meta: {
                        title: 'NestMining',
                        icon: 'mdi-gauge',
                        keepAlive: false
                    },
                    component: () => import('@/views/nest/index.vue')
                },
            ]
        },
        // 仪表盘
        {
            path: '/dashboard',
            visible: true,
            component: layout,
            meta: {
                title: 'dashboard',
                icon: 'mdi-gauge',
                keepAlive: false
            },
            children: [
                {
                    path: '',
                    name: 'dashboard',
                    meta: {
                        title: 'dashboard',
                        icon: 'mdi-gauge',
                        keepAlive: false
                    },
                    component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/index.vue')
                },
            ]
        },
        // 组件
        {
            path: '/components',
            visible: true,
            redirect: '/components/table',
            component: layout,
            meta: {
                title: 'components',
                icon: 'mdi-view-comfy',
                keepAlive: false
            },
            children: [
                {
                    path: 'widgets',
                    name: 'widget',
                    meta: {
                        title: 'widgets',
                        icon: 'mdi-alpha-w',
                        keepAlive: false
                    },
                    component: () => import(/* webpackChunkName: "table" */ '@/views/components/widget.vue')
                },
                {
                    path: 'sparklines',
                    name: 'sparklines',
                    meta: {
                        title: 'Sparklines',
                        icon: 'mdi-alpha-s',
                        keepAlive: false
                    },
                    component: () => import(/* webpackChunkName: "charts" */ '@/views/components/sparklines.vue')
                },
                {
                    path: 'form',
                    name: 'form',
                    meta: {
                        title: 'form',
                        icon: 'mdi-alpha-f',
                        keepAlive: false
                    },
                    component: () => import(/* webpackChunkName: "charts" */ '@/views/components/form.vue')
                },
                {
                    path: 'table',
                    name: 'starTask',
                    meta: {
                        title: 'table',
                        icon: 'mdi-alpha-t',
                        keepAlive: false
                    },
                    component: () => import(/* webpackChunkName: "starTask" */ '@/views/components/table.vue')
                }
            ]
        },
        // 登录页面
        {
            path: '/login',
            name: 'login',
            visible: true,
            meta: {
                title: 'login',
                icon: 'mdi-fingerprint',
                keepAlive: false
            },
            component: () => import(/* webpackChunkName: "login" */ '@/views/login/index.vue')
        },
        {
            path: '/404',
            name: '404',
            visible: false,
            component: () => import(/* webpackChunkName: "404page" */ '@/views/exception-page/404.vue'),
            meta: {
                title: '404',
                keepAlive: false
            }
        },
        { path: '*', redirect: '/404' }
    ],
    // scrollBehavior(to, from, savedPosition) {
    //     if (savedPosition) {
    //         return savedPosition;
    //     } else {
    //         return { x: 0, y: 0 };
    //     }
    // }
});

