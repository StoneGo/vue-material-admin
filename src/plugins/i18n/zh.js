
module.exports = {
    header: {
        dashboard: '仪表盘',
        task: '任务',
        components: 'UI组件',
        schedule: '日程',
        flies: '文件',
        map: '地图',
        flowEditor: '流程图编辑器',
        keepAlive: '页面缓存',
        login: '登录页'
    },
    task: {
        title: '我的任务'
    },
    files: {
        title: '文件',
        btn: '上传 Excel 文件'
    },
    thxtable: {
        thxhash: '事务哈希',
        coin: '币种',
        ntoken: 'nToken',
        action: '动作',
        turnout: '取回nToken',
        offer: '报价',
        gas: 'Gas费',
        status: '状态',
        from: '来源',
        eth:'ETH',
        erc20: 'ERC20',
        gastimes: 'Gas倍数'
    }
};
