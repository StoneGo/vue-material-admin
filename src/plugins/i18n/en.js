
module.exports = {
    header: {
        dashboard: 'Dashboard',
        task: 'Tasks',
        components: 'components',
        schedule: 'Schedule',
        flies: 'Flies',
        map: 'Map',
        flowEditor: 'Flow Editor',
        keepAlive: 'Keep Alive',
        login: 'Login Page'
    },
    task: {
        title: 'My task'
    },
    files: {
        title: 'files',
        btn: 'Upload excel '
    },
    thxtable: {
        thxhash: '事务哈希',
        coin: '币种',
        ntoken: 'nToken',
        action: '动作',
        turnout: '取回nToken',
        offer: '报价',
        gas: 'Gas费',
        status: '状态',
        from: '来源',
        eth:'ETH',
        erc20: 'ERC20',
        gastimes: 'Gas倍数'
    }
};
