\基于 Vue + Vuetify 实现的后台管理系统模板，使用 Material 设计风格，支持响应式移动端，支持颜色主题修改，多语言切换，黑白模式切换

## 目录结构
``` shell              　　
├─ public
├─ tests
├─ src
│  ├─ assets 
│  ├─ mixins 
│  ├─ plugins
│  ├─ router
│  ├─ styles
│  ├─ views
│  ├─ app.Vue 
│  └─ main.js
├─ .browserslistrc
├─ editorconfig
├─ .eslintrc.js
├─ .gitignore
├─ babel.config.js
├─ cypress.json
├─ babel.config.js
├─ LICENSE
├─ package.json
├─ README.md 
├─ vue.config
└─ webpack.config
```
